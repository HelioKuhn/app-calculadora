import { useEffect, useState } from 'react'
import './AppGorjeta.css'

function AppGorjeta() {
  const [pessoas, setPessoas] = useState(1);

  const [totalDaConta, setTotalConta] = useState(0);

  const [porcentagem, setPorcentagem] = useState(0);

  const [totalPessoa, setTotalPessoa] = useState(totalDaConta)

  const calcularTotalPorPessoa = () =>{
    if(totalDaConta !=""){
      let totalconta = parseInt(totalDaConta);
      let gorjeta =  porcentagem === "" ? 0 : parseInt(porcentagem)
      let totalPessoa = parseInt(pessoas)
      let total = (totalconta + (totalDaConta * (porcentagem/100)))/totalPessoa
    
      setTotalPessoa(total.toFixed(2))
    }
  }

  useEffect(calcularTotalPorPessoa,[totalDaConta,porcentagem,totalPessoa,pessoas])

  const addPessoas = () =>{
    setPessoas(p => p+=1)
  }

  const removePessoas = () =>{
    if(pessoas >1){
      setPessoas(p => p-=1)
    }
  }

  return (
    <main className="tela">
      <h2>App Calculadora</h2>
      <p>
        <label htmlFor="totalConta">Total da Conta</label>
      </p>
      <p>
        <input type="text" id="totalConta" placeholder='R$0.00' value={totalDaConta} onChange={(e) => setTotalConta(e.target.value)} ></input>
      </p>
      <p>
        <label htmlFor="gorjeta">Gorjeta</label>
      </p>  
      <p>
        <input type="text" id="gorjeta" placeholder='%Gorjeta' value={porcentagem} onChange={(e) => setPorcentagem(e.target.value)}></input>
      </p>
      <span>Pessoas</span> <span id='totalPorPessoa'>Total por Pessoa</span>
      <p><button onClick={removePessoas}>-</button>{pessoas}<button onClick={addPessoas}>+</button><span id='total'>{`R$${totalPessoa}`}</span></p>
    </main>
  )
}

export default AppGorjeta
