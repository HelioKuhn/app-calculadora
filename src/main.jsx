import React from 'react'
import ReactDOM from 'react-dom/client'
import AppGorjeta from './AppGorjeta'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AppGorjeta />
  </React.StrictMode>
)
